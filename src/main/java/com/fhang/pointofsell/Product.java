/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.pointofsell;

import java.util.ArrayList;

/**
 *
 * @author Natthakritta
 */
public class Product {
    private int id;
    private String name;
    private double price;

    public Product(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + '}';
    }
    
    public  static ArrayList<Product> getMockProductList(){
        ArrayList list = new ArrayList<Product>();
        for (int i = 0; i < 9; i++) {
            list.add(new Product(i+1,"Coffee"+ i+1,(i+1)*5));
        }
        
        return list;
    }
    
}
